import urlFilter from '../filters/urlFilter';

test('urlFilter should remove http or https from url', () => {
  expect(urlFilter('https://www.something.com',)).toBe('www.something.com');
  expect(urlFilter('http://something.com',)).toBe('something.com');
});

test('urlFilter should remove parameters from url', () => {
  expect(urlFilter('www.something.com?param=value')).toBe('www.something.com');
  expect(urlFilter('admin.something.com?param=value&more=things')).toBe('admin.something.com');
});

test('urlFilter should remove paths from url', () => {
  expect(urlFilter('www.something.com/path/somewhere?param=value')).toBe('www.something.com');
  expect(urlFilter('admin.something.com/somepath')).toBe('admin.something.com');
});
