import React from 'react'
import TopNavBar from '../components/TopNavBar';
import NewsList from '../components/NewsList';
import fetch from 'isomorphic-unfetch'

class Index extends React.Component {
  constructor() {
    super();
    this.state ={newsIds: []};
  };
  fetchNewsIds = () => {
    fetch(`https://hacker-news.firebaseio.com/v0/topstories.json?print=pretty`)
    .then( res => res.json() )
    .then( topNewsIds => {
      this.setState({newsIds: topNewsIds});
    });
  };
  componentDidMount() {
    this.fetchNewsIds();
  };
  render () {
    return(
        <div>
            <TopNavBar onRefresh={this.fetchNewsIds.bind(this)}/>
            <NewsList newsIds={this.state.newsIds}/>
        </div>
    )
  }

};

export default Index;
