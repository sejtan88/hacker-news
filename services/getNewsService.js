import fetch from 'isomorphic-unfetch';

const getSingleNews = async (id) => {
  const news = await fetch(`https://hacker-news.firebaseio.com/v0/item/${id}.json?print=pretty`);
  return await parseSingleNews(news);
};

const parseSingleNews = async (nws) => {
  return await nws.json();
};

const getNewsService = (ids) => {
  const news = Promise.all(ids.map( id => getSingleNews(id)));
  return news;
};

export default getNewsService;
