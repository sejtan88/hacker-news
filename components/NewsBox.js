import React from 'react';
import dateFilter from '../filters/dateFilter';
import urlFilter from '../filters/urlFilter';

const NewsBox = (props) => (
  <div className={props.first ? "newsbox newsbox--first" : "newsbox"}>
      <div className="newsbox__index">{props.index}.</div>
      <div className="newsbox__data">
        <div className="newsbox__data__title">
          <span className="newsbox--black">{props.ntitle}</span>
          <span className="newsbox--gray">{props.url ? ' (' + urlFilter(props.url) + ')' : ''}</span>
        </div>
        <div className="newsbox__data__comments">
          <span className="newsbox--black">{props.score + ' points'}</span>
          <span className="newsbox--gray"> by </span>
          <span className="newsbox--black">{props.by}</span>
          <span className="newsbox--gray">{props.time ? ' ' + dateFilter(props.time) + ' | ' : ''}</span>
          <span className="newsbox--black">{props.descendants ? props.descendants + ' comments': ''}</span>
        </div>
      </div>
      <style jsx>{`
          .newsbox.newsbox--first {
              border-width: 0 0 1px 0;
          }
          .newsbox {
            min-width: 780px;
            padding: 10px;
            border-style: solid;
            border-width: 0 0 1px 0;
            border-color: rgba(51,51,51, 0.2);
            display: flex;
            align-items: center;
            justify-content: flex-start;
          }
          .newsbox__index {
            min-width: 20px;
            padding: 5px;
            color: grey;
            opacity: 0.8;
          }
          .newsbox__data {
            display: flex;
            flex-direction: column;
            align-items: flex-start;
            justify-content: center;
          }
          .newsbox__data__title {
            font-size: 16px;
          }
          .newsbox__data__comments {
            font-size: 14px;
          }
          .newsbox--black {
            color: black;
          }
          .newsbox--gray {
            color: gray;
          }
      `}</style>
  </div>
);

export default NewsBox;
