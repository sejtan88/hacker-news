import React from 'react';
import NewsBox from './NewsBox';
import getNewsService from '../services/getNewsService';


class NewsList extends React.Component {

  constructor(props) {
    super();
    this.state = {news: [], page: 0};
  };

  fetchNewsList = (page, list) => {
    const ids = list.slice(page*10, page*10+10);
    getNewsService(ids).then(res => {
      this.setState({news: res, page: page});
    });
  };
  componentDidMount() {
    this.fetchNewsList(this.state.page || 0, this.props.newsIds);
  };
  componentWillReceiveProps(props) {
    this.fetchNewsList(0, props.newsIds);
  };
  renderNews = (nws, i) => <NewsBox key={i} index={this.state.page*10+1+i} ntitle={nws.title} url={nws.url} by={nws.by} score={nws.score} time={nws.time} descendants={nws.descendants} first={i===0 ? true : false}/>;
  loadMore = () => {
    this.fetchNewsList(this.state.page+1, this.props.newsIds);
  };
  loadPrev = () => {
    this.fetchNewsList(this.state.page-1, this.props.newsIds);
  };
  render() {
  return(
    <div>
      <div className="news-list">
        {this.state && this.state.news && this.state.news.map(this.renderNews)}
      </div>
      <div className="news-navigation">
        {this.state.page ? <div onClick={this.loadPrev}>Prev</div> : ''}
        {this.state.page ? <span>|</span> : ''}
        <div onClick={this.loadMore}>More</div>
      </div>
      <style jsx>{`
        .news-navigation {
          display: flex;
          justify-content: flex-start;
          align-items: center;
          padding: 15px;
          font-size: 14px;
        }
        .news-list {
          padding: 0 15px;
        }
        .news-navigation div {
          cursor: pointer;
          margin: 0 10px;
        }
      `}</style>
    </div>
    )
  };
}

export default NewsList;
